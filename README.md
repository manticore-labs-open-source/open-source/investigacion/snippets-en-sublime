# Creación y uso de snippets en Sublime Text

## Sublime Text

Sublime Text no es más que un editor de código escrito en los lenguajes C++ y Python para el uso de pluggins. 
Originalmente fue creado como una extensión del editor de código Vim (Linux) pero con el tiempo fue tomando su propia identidad.
Es un editor multiplataforma, de sencilla instalación y puede utilizarse de manera gratuita, puede descargarse desde su página oficial dando clic [aquí](https://www.sublimetext.com/) 

## ¿Qué son los snippets?

En pocas palabras, un snippet no es más que ayuda para los programadores, una forma de reutilizar pequeñas partes de código sin necesidad de copiar y pegar.
Pueden tratarse de módulos, componentes, funciones o incluso servicios cuya estructura pueda ser reutilizada de manera sencilla en módulos mucho más grandes, aportando funcionalidad, por supuesto.
Para utilizar un snippet, basta con ingresar la palabra clave representativa de dicho snippet y presionar la tecla tab del teclado.

## Creación de snippets

Dentro del editor de Sublime Text es necesario ir a la siguiente ruta: *_Tools > Developer > New snippet_* y mostrará dentro del editor un archivo similar al apreciado a continuación.

![Creación del snippet](./imagenes/creacion-snippet.png)

Ahora bien, antes de continuar, es necesario señalar las distintas partes que tiene el snippet creado, y estas son:

*  **Contenido (obligatorio):** Dentro del tag <*_content_*>, específicamente entre <*_![CDATA[_* y *_]]_*> es donde se define la porción de código deseada. Se pueden definir campos con la sintaxis **$1**, placeholders a través de **${1: variable}**. Un pequeño tip es el de utilizar **$0** para indicar exactamente donde debe terminar el cursor.
*  **La palabra "disparador" (recomendado):** Se trata de la palabra que hará que se ejecute el snippet, dicha palabra puede definirse dentro del tag <*_tabTrigger_*>. Es recomendable ya que en el caso de existir múltiplos snippets similares, la palabra ayudará a escoger el snippet adecuado para su uso.
*  **Alcance (opcional):** Identificado por el tag <*_scope_*>, permite limitar, como su nombre lo indica, el alcance el snippet. Por ejemplo, si se quiere crear un snippet para el bucle if solamente para JavaScript, se tendría que usar **source.js** dentro del tag para que el snippet sea inútil en otros lenguajes.
*  **Comentarios (opcional):** Como se puede notar en la imagen anterior, esta opción no se muestra por alguna razón, sin embargo puede ser utilizada a través del tag <*_description_*> y lo que hace es dar una pequeña descripción de la finalidad del snippet al momento de invocarlo.

Con todos los elementos claros, se procede a crear un snippet sencillo para la creación de componentes en Angular, para lo cual, el snippet luce así:

![Creación del snippet para componentes Angular](./imagenes/snippet-componente.png)

Una vez finalizada la creación del snippet, se procede a guardarlo para su posterior uso. En Mac OS, por defecto, se almacenará en la ruta: *_~/Library/Application Support/Sublime Text 3/Packages/User_*.
Es importante, al momento de guardar el archivo, hacerlo con la extensión **.sublime-snippet** o caso contrario, no será posible utilizarlo.

## Utilización del snippet

Con el snippet ya creado y almacenado correctamente, sólo resta probarlo. Para el caso de prueba, se ha creado un archivo llamado **usuario.component.ts** y al escribir la palabra clave, se puede observar que ya aparece el snippet.

![Funcionamiento del snippet](./imagenes/funcionalidad-snippet.png)

Al escoger el snippet, se llenará el archivo con el contenido definido en el mismo así:

![Archivo creado con el snippet](./imagenes/archivo-creado.png)

El paso final consiste en llenar las secciones resaltadas que corresponden a los placeholder definidos en el snippet.

## Ventajas de uso dentro del editor

Una de las mayores desventajas de Sublime Text es que a diferencia de otros editores como Visual Studio, es que no se generan los imports de manera automática, así pues, utilizando snippets, podría reducirse el trabajo de importar variables o elementos de una librería de forma manual.

<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>
